<?php

use Illuminate\Database\Seeder;

class PerritoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Perrito::class, 20)->create();
    }
}
