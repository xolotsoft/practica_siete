<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Perrito;
use Faker\Generator as Faker;

$factory->define(Perrito::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'address' => $faker->address,
        'race' => $faker->lastName
    ];
});
